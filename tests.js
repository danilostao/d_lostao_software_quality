
test("Valid Plate", function (assert) {

    assert.equal(isValidPlate('0980FDV'), true, "VALID");

    assert.equal(isValidPlate('9424HJJ'), true, "VALID");

    assert.equal(isValidPlate('6666MBV'), true, "VALID");

    assert.equal(isValidPlate('4567fvc'), true,"VALID");

    assert.equal(isValidPlate('1235TsC'), true, "VALID");

  
});

test("Not valid plate", function (assert) {

    assert.equal(isValidPlate('1234ÑPL'), false, "IS INVALID Ñ LETTER");

    assert.equal(isValidPlate('5555PRQ'), false, "IS INVALID Q LETTER");

    assert.equal(isValidPlate('0980.FDV'), false, "IS INVALID USING PUNTUATION");

    assert.equal(isValidPlate('43PR4T5'), false, "IS INVALID PLATE DISORDERED");

    assert.equal(isValidPlate('9898 HJJ'), false, "IS INVALID USING SPACE");

    assert.equal(isValidPlate('123LBDC'), false, "IS INVALID NOT ENOUGH LETTERS AND TOO LETTERS");

    assert.equal(isValidPlate('333LLL'), false, "IS INVALID NOT ENOUGH LETTERS AND NOT ENOUGH NUMBERS");
    
    assert.equal(isValidPlate('115LBC'), false, "IS INVALID NOT ENOUGH LETTERS");

    assert.equal(isValidPlate('113LRT'), false, "IS INVALID NOT ENOUGH NUMBERS");

    assert.equal(isValidPlate('8909854'), false, "IS INVALID ONLY NUMBERS");

    assert.equal(isValidPlate('DFGHLLP'), false, "IS INVALID ONLY LETTERS");

    assert.equal(isValidPlate('2345ACL'), false, "IS INVALID USING VOWEL A");


});




